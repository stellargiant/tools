// ErrorMsg.h: interface for the CErrorMsg class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ERRORMSG_H__D49403EC_2D8D_40BE_8CC1_4468224CF94C__INCLUDED_)
#define AFX_ERRORMSG_H__D49403EC_2D8D_40BE_8CC1_4468224CF94C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CErrorMsg  
{
public:
	static CString GetErrorString();
	CErrorMsg();
	virtual ~CErrorMsg();

};

#endif // !defined(AFX_ERRORMSG_H__D49403EC_2D8D_40BE_8CC1_4468224CF94C__INCLUDED_)
