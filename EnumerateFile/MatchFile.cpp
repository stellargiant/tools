#include "StdAfx.h"
#include "MatchFile.h"

CMatchFile::CMatchFile(void)
{
}

CMatchFile::~CMatchFile(void)
{
}

bool CMatchFile::IsMatchFile(LPCTSTR lpzFile,LPCTSTR lpzWild)
{
	LPCTSTR cp = NULL, mp = NULL;

	while (*lpzFile && (*lpzWild != L'*')) 
	{
		if ((toupper(*lpzWild) != toupper(*lpzFile)) && (*lpzWild != L'?')) 
		{
			return false;
		}

    lpzWild++;
    lpzFile++;

	}

	while (*lpzFile)
	{
		if (*lpzWild == L'*') 
		{
			if (!*++lpzWild) 
			{
				return true;
			}
			mp = lpzWild;
			cp = lpzFile+1;
		}
		else if ((toupper(*lpzWild) == toupper(*lpzFile)) || (*lpzFile == '?')) 
		{
			lpzWild++;
			lpzFile++;
		} 
		else 
		{
			lpzWild = mp;
			lpzFile = cp++;
		}
	}

	while (*lpzWild == L'*') 
	{
		lpzWild++;
	}

	return !*lpzWild;
}

bool CMatchFile::IsMatchExtension(LPCTSTR lpzFile, LPCTSTR lpzExt)
{
	_tcscpy_s(m_ctemp,lpzExt);
	TCHAR *token;
	TCHAR *nextToken;
	token = _tcstok_s( m_ctemp, _T(","), &nextToken );
	
	m_sArrayExt.RemoveAll();
		
	while(token!=NULL)
	{
		m_sArrayExt.Add(token);
	    token = _tcstok_s( NULL, _T(","), &nextToken );
	}
	bool bFound=false;

	for (int i=0;i<m_sArrayExt.GetSize();i++) 
	{
		m_sCompare=m_sArrayExt[i];
		if(!m_sCompare.CompareNoCase(_T("*.*")))
		{
			bFound=true;
			break;
		}

		m_sCompare=GetFileExtension(m_sCompare);
		if(!m_sCompare.CompareNoCase(GetFileExtension(lpzFile)))
		{
			bFound=true;
			break;
		}
	}
	return bFound;

}

CString CMatchFile::GetFileExtension(LPCTSTR lpzFile)
{
	CString sFile(lpzFile);
	CString sExtension;
	int i=sFile.ReverseFind('.');
	sExtension=sFile.Mid(i+1);
//	TRACE("Extension=%s\n",sExtension);
	
	return sExtension;

}
