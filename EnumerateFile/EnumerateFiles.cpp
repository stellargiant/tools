// EnumerateFiles.cpp: implementation of the CEnumerateFiles class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "EnumerateFiles.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CEnumerateFiles::CEnumerateFiles()
{
	m_DirBeforeRecurse = true;
	m_DirAfterRecurse = false;
}

CEnumerateFiles::~CEnumerateFiles()
{

}

void CEnumerateFiles::Execute(LPCTSTR lpzDir,LPCTSTR lpzMatch, bool bRecurse, bool bWildMatch)
{
	CFileFind FindAllFile;
	bool continueEnumerate = true;

	CString sFileName;
	if (lpzDir == NULL)
	{
		sFileName = _T("*.*"); // to traverse to next directory 
	}
	else
	{
		sFileName = lpzDir;
		sFileName += _T("\\*.*"); // to traverse to next directory 
	}

	BOOL bFileExist=FindAllFile.FindFile(sFileName);

	while(bFileExist && continueEnumerate)
	{
		bFileExist=FindAllFile.FindNextFile();

		if(FindAllFile.IsDots())
			continue;
		
		if(FindAllFile.IsDirectory() && bRecurse)
		{
			if (m_DirBeforeRecurse)
			{
				continueEnumerate = ExecuteDirectory(FindAllFile.GetFilePath());
			}

			if (continueEnumerate)
			{
				Execute(FindAllFile.GetFilePath(),lpzMatch,bRecurse);
			}
			continue;
		}

		if ((!bWildMatch && m_MatchFile.IsMatchExtension(FindAllFile.GetFilePath(),lpzMatch)) ||
			(bWildMatch && m_MatchFile.IsMatchFile(FindAllFile.GetFileName(), lpzMatch)))
		{
			continueEnumerate = OnFile(FindAllFile.GetFilePath());
		}

	}

	FindAllFile.Close();

	if ((lpzDir != NULL) && m_DirAfterRecurse && continueEnumerate)
	{
		ExecuteDirectory(lpzDir);
	}
		
}



bool CEnumerateFiles::OnFile(LPCTSTR lpzFile)
{
	TRACE("FileName=%s\n",lpzFile);
	return true;
}

bool CEnumerateFiles::ExecuteDirectory(LPCTSTR lpzDir)
{
	return true;
}
