// EnumerateFiles.h: interface for the CEnumerateFiles class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ENUMERATEFILES_H__4FED7690_DFF3_4ACC_AB75_DD0B385B8C54__INCLUDED_)
#define AFX_ENUMERATEFILES_H__4FED7690_DFF3_4ACC_AB75_DD0B385B8C54__INCLUDED_

#include "MatchFile.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CEnumerateFiles  
{
public:
	CEnumerateFiles();
	virtual ~CEnumerateFiles();

	virtual void Execute(LPCTSTR lpzDir,LPCTSTR lpzExt, bool bRecurse=true, bool wildMatch = false);
	void SetDirExecuteBeforeRecurse(bool before)
	{
		m_DirBeforeRecurse = before;
	}

	void SetDirExecuteAfterRecures(bool after)
	{
		m_DirAfterRecurse = after;
	}


protected:
	CMatchFile m_MatchFile;
	virtual bool ExecuteDirectory(LPCTSTR lpzDir);
	virtual bool OnFile(LPCTSTR lpzFile)=0;
	bool m_DirBeforeRecurse;
	bool m_DirAfterRecurse;
	
};

#endif // !defined(AFX_ENUMERATEFILES_H__4FED7690_DFF3_4ACC_AB75_DD0B385B8C54__INCLUDED_)
