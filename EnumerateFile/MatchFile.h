#ifndef MATCHFILE_H
#define MATCHFILE_H

class CMatchFile
{
	CString GetFileExtension(LPCTSTR lpzFile);
	CStringArray m_sArrayExt;
	TCHAR m_ctemp[1024];
	CString m_sCompare;
public:
	virtual bool IsMatchExtension(LPCTSTR lpzFile,LPCTSTR lpzExt);
	virtual bool IsMatchFile(LPCTSTR lpzFile,LPCTSTR lpzWild);
	CMatchFile(void);
	virtual ~CMatchFile(void);

	
};

#endif	//#ifndef MATCHFILE_H
