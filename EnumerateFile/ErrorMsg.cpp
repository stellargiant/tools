// ErrorMsg.cpp: implementation of the CErrorMsg class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ErrorMsg.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CErrorMsg::CErrorMsg()
{

}

CErrorMsg::~CErrorMsg()
{

}

CString CErrorMsg::GetErrorString()
{
	CString m_sErrorMsg;

	LPVOID lpMsgBuf;
	FormatMessage( 
		FORMAT_MESSAGE_ALLOCATE_BUFFER | 
		FORMAT_MESSAGE_FROM_SYSTEM | 
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		GetLastError(),
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
// Default language
		(LPTSTR) &lpMsgBuf,
		0,
		NULL 
	);
	// Process any inserts in lpMsgBuf.
	// ...
	//
	//Display the string.

	m_sErrorMsg=(LPCTSTR)lpMsgBuf;
	
	TRACE("%s\n",(LPCTSTR)lpMsgBuf);
	// Free the buffer.
	LocalFree( lpMsgBuf );

	return m_sErrorMsg;
}
