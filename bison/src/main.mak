# Microsoft Developer Studio Generated NMAKE File, Format Version 40001
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

!IF "$(CFG)" == ""
CFG=main - Win32 Debug
!MESSAGE No configuration specified.  Defaulting to main - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "main - Win32 Release" && "$(CFG)" != "main - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE on this makefile
!MESSAGE by defining the macro CFG on the command line.  For example:
!MESSAGE 
!MESSAGE NMAKE /f "main.mak" CFG="main - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "main - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "main - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 
################################################################################
# Begin Project
# PROP Target_Last_Scanned "main - Win32 Debug"
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "main - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
OUTDIR=.
INTDIR=.

ALL : "$(OUTDIR)\bison.exe"

CLEAN : 
	-@erase ".\bison.exe"
	-@erase ".\conflicts.obj"
	-@erase ".\warshall.obj"
	-@erase ".\allocate.obj"
	-@erase ".\main.obj"
	-@erase ".\lex.obj"
	-@erase ".\reduce.obj"
	-@erase ".\symtab.obj"
	-@erase ".\output.obj"
	-@erase ".\LR0.obj"
	-@erase ".\print.obj"
	-@erase ".\gram.obj"
	-@erase ".\nullable.obj"
	-@erase ".\derives.obj"
	-@erase ".\getopt1.obj"
	-@erase ".\getopt.obj"
	-@erase ".\files.obj"
	-@erase ".\closure.obj"
	-@erase ".\lalr.obj"
	-@erase ".\reader.obj"
	-@erase ".\getargs.obj"

# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /YX /c
# ADD CPP /nologo /W3 /GX /O2 /I "C:\Vera32\Others\bygpl\bison\bison-1.28\src" /D "NDEBUG" /D "WIN32" /D "_CONSOLE" /D "STDC_HEADERS" /D "HAVE_CONFIG_H" /D "HAVE_STRING_H" /D "HAVE_MALLOC_H" /YX /c
# SUBTRACT CPP /X
CPP_PROJ=/nologo /ML /W3 /GX /O2 /I\
 "C:\Vera32\Others\bygpl\bison\bison-1.28\src" /D "NDEBUG" /D "WIN32" /D\
 "_CONSOLE" /D "STDC_HEADERS" /D "HAVE_CONFIG_H" /D "HAVE_STRING_H" /D\
 "HAVE_MALLOC_H" /Fp"main.pch" /YX /c 
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/main.bsc" 
BSC32_SBRS=
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386 /out:"bison.exe"
# SUBTRACT LINK32 /nodefaultlib
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib\
 advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib\
 odbccp32.lib /nologo /subsystem:console /incremental:no\
 /pdb:"$(OUTDIR)/bison.pdb" /machine:I386 /out:"$(OUTDIR)/bison.exe" 
LINK32_OBJS= \
	"$(INTDIR)/conflicts.obj" \
	"$(INTDIR)/warshall.obj" \
	"$(INTDIR)/allocate.obj" \
	"$(INTDIR)/main.obj" \
	"$(INTDIR)/lex.obj" \
	"$(INTDIR)/reduce.obj" \
	"$(INTDIR)/symtab.obj" \
	"$(INTDIR)/output.obj" \
	"$(INTDIR)/LR0.obj" \
	"$(INTDIR)/print.obj" \
	"$(INTDIR)/gram.obj" \
	"$(INTDIR)/nullable.obj" \
	"$(INTDIR)/derives.obj" \
	"$(INTDIR)/getopt1.obj" \
	"$(INTDIR)/getopt.obj" \
	"$(INTDIR)/files.obj" \
	"$(INTDIR)/closure.obj" \
	"$(INTDIR)/lalr.obj" \
	"$(INTDIR)/reader.obj" \
	"$(INTDIR)/getargs.obj"

"$(OUTDIR)\bison.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "main - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
OUTDIR=.
INTDIR=.

ALL : "$(OUTDIR)\bison.exe"

CLEAN : 
	-@erase ".\vc40.pdb"
	-@erase ".\vc40.idb"
	-@erase ".\bison.exe"
	-@erase ".\conflicts.obj"
	-@erase ".\warshall.obj"
	-@erase ".\allocate.obj"
	-@erase ".\main.obj"
	-@erase ".\lex.obj"
	-@erase ".\reduce.obj"
	-@erase ".\symtab.obj"
	-@erase ".\output.obj"
	-@erase ".\LR0.obj"
	-@erase ".\print.obj"
	-@erase ".\gram.obj"
	-@erase ".\nullable.obj"
	-@erase ".\derives.obj"
	-@erase ".\getopt1.obj"
	-@erase ".\getopt.obj"
	-@erase ".\files.obj"
	-@erase ".\closure.obj"
	-@erase ".\lalr.obj"
	-@erase ".\reader.obj"
	-@erase ".\getargs.obj"
	-@erase ".\bison.ilk"
	-@erase ".\bison.pdb"

# ADD BASE CPP /nologo /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /YX /c
# ADD CPP /nologo /W3 /Gm /GX /Zi /Od /I "C:\Vera32\Others\bygpl\bison\bison-1.28\src" /D "_DEBUG" /D "WIN32" /D "_CONSOLE" /D "STDC_HEADERS" /D "HAVE_CONFIG_H" /D "HAVE_STRING_H" /D "HAVE_MALLOC_H" /YX /c
# SUBTRACT CPP /X
CPP_PROJ=/nologo /MLd /W3 /Gm /GX /Zi /Od /I\
 "C:\Vera32\Others\bygpl\bison\bison-1.28\src" /D "_DEBUG" /D "WIN32" /D\
 "_CONSOLE" /D "STDC_HEADERS" /D "HAVE_CONFIG_H" /D "HAVE_STRING_H" /D\
 "HAVE_MALLOC_H" /Fp"main.pch" /YX /c 
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/main.bsc" 
BSC32_SBRS=
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /out:"bison.exe"
# SUBTRACT LINK32 /nodefaultlib
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib\
 advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib\
 odbccp32.lib /nologo /subsystem:console /incremental:yes\
 /pdb:"$(OUTDIR)/bison.pdb" /debug /machine:I386 /out:"$(OUTDIR)/bison.exe" 
LINK32_OBJS= \
	"$(INTDIR)/conflicts.obj" \
	"$(INTDIR)/warshall.obj" \
	"$(INTDIR)/allocate.obj" \
	"$(INTDIR)/main.obj" \
	"$(INTDIR)/lex.obj" \
	"$(INTDIR)/reduce.obj" \
	"$(INTDIR)/symtab.obj" \
	"$(INTDIR)/output.obj" \
	"$(INTDIR)/LR0.obj" \
	"$(INTDIR)/print.obj" \
	"$(INTDIR)/gram.obj" \
	"$(INTDIR)/nullable.obj" \
	"$(INTDIR)/derives.obj" \
	"$(INTDIR)/getopt1.obj" \
	"$(INTDIR)/getopt.obj" \
	"$(INTDIR)/files.obj" \
	"$(INTDIR)/closure.obj" \
	"$(INTDIR)/lalr.obj" \
	"$(INTDIR)/reader.obj" \
	"$(INTDIR)/getargs.obj"

"$(OUTDIR)\bison.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c.obj:
   $(CPP) $(CPP_PROJ) $<  

.cpp.obj:
   $(CPP) $(CPP_PROJ) $<  

.cxx.obj:
   $(CPP) $(CPP_PROJ) $<  

.c.sbr:
   $(CPP) $(CPP_PROJ) $<  

.cpp.sbr:
   $(CPP) $(CPP_PROJ) $<  

.cxx.sbr:
   $(CPP) $(CPP_PROJ) $<  

################################################################################
# Begin Target

# Name "main - Win32 Release"
# Name "main - Win32 Debug"

!IF  "$(CFG)" == "main - Win32 Release"

!ELSEIF  "$(CFG)" == "main - Win32 Debug"

!ENDIF 

################################################################################
# Begin Source File

SOURCE=.\main.c
DEP_CPP_MAIN_=\
	".\system.h"\
	".\machine.h"\
	".\config.h"\
	

"$(INTDIR)\main.obj" : $(SOURCE) $(DEP_CPP_MAIN_) "$(INTDIR)"


# End Source File
################################################################################
# Begin Source File

SOURCE=.\closure.c
DEP_CPP_CLOSU=\
	".\system.h"\
	".\machine.h"\
	".\alloc.h"\
	".\gram.h"\
	".\config.h"\
	

"$(INTDIR)\closure.obj" : $(SOURCE) $(DEP_CPP_CLOSU) "$(INTDIR)"


# End Source File
################################################################################
# Begin Source File

SOURCE=.\conflicts.c
DEP_CPP_CONFL=\
	".\system.h"\
	".\machine.h"\
	".\alloc.h"\
	".\files.h"\
	".\gram.h"\
	".\state.h"\
	".\config.h"\
	

"$(INTDIR)\conflicts.obj" : $(SOURCE) $(DEP_CPP_CONFL) "$(INTDIR)"


# End Source File
################################################################################
# Begin Source File

SOURCE=.\derives.c
DEP_CPP_DERIV=\
	".\system.h"\
	".\alloc.h"\
	".\types.h"\
	".\gram.h"\
	".\config.h"\
	

"$(INTDIR)\derives.obj" : $(SOURCE) $(DEP_CPP_DERIV) "$(INTDIR)"


# End Source File
################################################################################
# Begin Source File

SOURCE=.\files.c
DEP_CPP_FILES=\
	".\system.h"\
	".\files.h"\
	".\alloc.h"\
	".\gram.h"\
	".\config.h"\
	

"$(INTDIR)\files.obj" : $(SOURCE) $(DEP_CPP_FILES) "$(INTDIR)"


# End Source File
################################################################################
# Begin Source File

SOURCE=.\getargs.c
DEP_CPP_GETAR=\
	".\getopt.h"\
	".\system.h"\
	".\files.h"\
	".\config.h"\
	

"$(INTDIR)\getargs.obj" : $(SOURCE) $(DEP_CPP_GETAR) "$(INTDIR)"


# End Source File
################################################################################
# Begin Source File

SOURCE=.\getopt.c
DEP_CPP_GETOP=\
	".\config.h"\
	".\getopt.h"\
	

"$(INTDIR)\getopt.obj" : $(SOURCE) $(DEP_CPP_GETOP) "$(INTDIR)"


# End Source File
################################################################################
# Begin Source File

SOURCE=.\getopt1.c
DEP_CPP_GETOPT=\
	".\config.h"\
	".\getopt.h"\
	

"$(INTDIR)\getopt1.obj" : $(SOURCE) $(DEP_CPP_GETOPT) "$(INTDIR)"


# End Source File
################################################################################
# Begin Source File

SOURCE=.\gram.c

"$(INTDIR)\gram.obj" : $(SOURCE) "$(INTDIR)"


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lalr.c
DEP_CPP_LALR_=\
	".\system.h"\
	".\machine.h"\
	".\types.h"\
	".\state.h"\
	".\alloc.h"\
	".\gram.h"\
	".\config.h"\
	

"$(INTDIR)\lalr.obj" : $(SOURCE) $(DEP_CPP_LALR_) "$(INTDIR)"


# End Source File
################################################################################
# Begin Source File

SOURCE=.\lex.c
DEP_CPP_LEX_C=\
	".\system.h"\
	".\files.h"\
	".\getopt.h"\
	".\symtab.h"\
	".\lex.h"\
	".\alloc.h"\
	".\config.h"\
	

"$(INTDIR)\lex.obj" : $(SOURCE) $(DEP_CPP_LEX_C) "$(INTDIR)"


# End Source File
################################################################################
# Begin Source File

SOURCE=.\LR0.c
DEP_CPP_LR0_C=\
	".\system.h"\
	".\machine.h"\
	".\alloc.h"\
	".\gram.h"\
	".\state.h"\
	".\config.h"\
	

"$(INTDIR)\LR0.obj" : $(SOURCE) $(DEP_CPP_LR0_C) "$(INTDIR)"


# End Source File
################################################################################
# Begin Source File

SOURCE=.\nullable.c
DEP_CPP_NULLA=\
	".\system.h"\
	".\types.h"\
	".\gram.h"\
	".\alloc.h"\
	".\config.h"\
	

"$(INTDIR)\nullable.obj" : $(SOURCE) $(DEP_CPP_NULLA) "$(INTDIR)"


# End Source File
################################################################################
# Begin Source File

SOURCE=.\output.c
DEP_CPP_OUTPU=\
	".\system.h"\
	".\machine.h"\
	".\alloc.h"\
	".\files.h"\
	".\gram.h"\
	".\state.h"\
	".\config.h"\
	

"$(INTDIR)\output.obj" : $(SOURCE) $(DEP_CPP_OUTPU) "$(INTDIR)"


# End Source File
################################################################################
# Begin Source File

SOURCE=.\print.c
DEP_CPP_PRINT=\
	".\system.h"\
	".\machine.h"\
	".\alloc.h"\
	".\files.h"\
	".\gram.h"\
	".\state.h"\
	".\config.h"\
	

"$(INTDIR)\print.obj" : $(SOURCE) $(DEP_CPP_PRINT) "$(INTDIR)"


# End Source File
################################################################################
# Begin Source File

SOURCE=.\reader.c
DEP_CPP_READE=\
	".\system.h"\
	".\files.h"\
	".\alloc.h"\
	".\symtab.h"\
	".\lex.h"\
	".\gram.h"\
	".\machine.h"\
	".\config.h"\
	

"$(INTDIR)\reader.obj" : $(SOURCE) $(DEP_CPP_READE) "$(INTDIR)"


# End Source File
################################################################################
# Begin Source File

SOURCE=.\reduce.c
DEP_CPP_REDUC=\
	".\system.h"\
	".\files.h"\
	".\gram.h"\
	".\machine.h"\
	".\alloc.h"\
	".\config.h"\
	

"$(INTDIR)\reduce.obj" : $(SOURCE) $(DEP_CPP_REDUC) "$(INTDIR)"


# End Source File
################################################################################
# Begin Source File

SOURCE=.\symtab.c
DEP_CPP_SYMTA=\
	".\system.h"\
	".\alloc.h"\
	".\symtab.h"\
	".\gram.h"\
	".\config.h"\
	

"$(INTDIR)\symtab.obj" : $(SOURCE) $(DEP_CPP_SYMTA) "$(INTDIR)"


# End Source File
################################################################################
# Begin Source File

SOURCE=.\warshall.c
DEP_CPP_WARSH=\
	".\system.h"\
	".\machine.h"\
	".\config.h"\
	

"$(INTDIR)\warshall.obj" : $(SOURCE) $(DEP_CPP_WARSH) "$(INTDIR)"


# End Source File
################################################################################
# Begin Source File

SOURCE=.\allocate.c
DEP_CPP_ALLOC=\
	".\system.h"\
	".\config.h"\
	

"$(INTDIR)\allocate.obj" : $(SOURCE) $(DEP_CPP_ALLOC) "$(INTDIR)"


# End Source File
# End Target
# End Project
################################################################################
