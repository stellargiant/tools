# Microsoft Developer Studio Project File - Name="main" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=main - Win32 Release
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "main.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "main.mak" CFG="main - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "main - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "main - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName "khurley-desk3"
# PROP Scc_LocalPath "."
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "main - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /YX /c
# ADD CPP /nologo /W3 /GX /O2 /I ".\\" /D "NDEBUG" /D "WIN32" /D "_CONSOLE" /D "STDC_HEADERS" /D "HAVE_CONFIG_H" /D "HAVE_STRING_H" /D "HAVE_MALLOC_H" /YX /FD /c
# SUBTRACT CPP /X
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386 /out:"bison.exe"
# SUBTRACT LINK32 /nodefaultlib

!ELSEIF  "$(CFG)" == "main - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# ADD BASE CPP /nologo /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /YX /c
# ADD CPP /nologo /W3 /Gm /GX /ZI /Od /I ".\\" /D "_DEBUG" /D "WIN32" /D "_CONSOLE" /D "STDC_HEADERS" /D "HAVE_CONFIG_H" /D "HAVE_STRING_H" /D "HAVE_MALLOC_H" /YX /FD /c
# SUBTRACT CPP /X
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /out:"bison.exe"
# SUBTRACT LINK32 /nodefaultlib

!ENDIF 

# Begin Target

# Name "main - Win32 Release"
# Name "main - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat;for;f90"
# Begin Source File

SOURCE=.\allocate.c
# ADD CPP /I ".\\"
# End Source File
# Begin Source File

SOURCE=.\closure.c
# ADD CPP /I ".\\"
# End Source File
# Begin Source File

SOURCE=.\conflicts.c
# ADD CPP /I ".\\"
# End Source File
# Begin Source File

SOURCE=.\derives.c
# ADD CPP /I ".\\"
# End Source File
# Begin Source File

SOURCE=.\files.c
# ADD CPP /I ".\\"
# End Source File
# Begin Source File

SOURCE=.\getargs.c
# ADD CPP /I ".\\"
# End Source File
# Begin Source File

SOURCE=.\getopt.c
# ADD CPP /I ".\\"
# End Source File
# Begin Source File

SOURCE=.\getopt1.c
# ADD CPP /I ".\\"
# End Source File
# Begin Source File

SOURCE=.\gram.c
# ADD CPP /I ".\\"
# End Source File
# Begin Source File

SOURCE=.\lalr.c
# ADD CPP /I ".\\"
# End Source File
# Begin Source File

SOURCE=.\lex.c
# ADD CPP /I ".\\"
# End Source File
# Begin Source File

SOURCE=.\LR0.c
# ADD CPP /I ".\\"
# End Source File
# Begin Source File

SOURCE=.\main.c
# ADD CPP /I ".\\"
# End Source File
# Begin Source File

SOURCE=.\nullable.c
# ADD CPP /I ".\\"
# End Source File
# Begin Source File

SOURCE=.\output.c
# ADD CPP /I ".\\"
# End Source File
# Begin Source File

SOURCE=.\print.c
# ADD CPP /I ".\\"
# End Source File
# Begin Source File

SOURCE=.\reader.c
# ADD CPP /I ".\\"
# End Source File
# Begin Source File

SOURCE=.\reduce.c
# ADD CPP /I ".\\"
# End Source File
# Begin Source File

SOURCE=.\symtab.c
# ADD CPP /I ".\\"
# End Source File
# Begin Source File

SOURCE=.\warshall.c
# ADD CPP /I ".\\"
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl;fi;fd"
# Begin Source File

SOURCE=.\alloc.h
# End Source File
# Begin Source File

SOURCE=.\config.h
# End Source File
# Begin Source File

SOURCE=.\files.h
# End Source File
# Begin Source File

SOURCE=.\getopt.h
# End Source File
# Begin Source File

SOURCE=.\gram.h
# End Source File
# Begin Source File

SOURCE=.\lex.h
# End Source File
# Begin Source File

SOURCE=.\machine.h
# End Source File
# Begin Source File

SOURCE=.\state.h
# End Source File
# Begin Source File

SOURCE=.\symtab.h
# End Source File
# Begin Source File

SOURCE=.\system.h
# End Source File
# Begin Source File

SOURCE=.\types.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;cnt;rtf;gif;jpg;jpeg;jpe"
# End Group
# End Target
# End Project
