#ifndef CSWIZZLETEXTURE_H__
#define CSWIZZLETEXTURE_H__

#define MAX_CACHE 8192
typedef unsigned char       u8;
typedef unsigned long       u32;
typedef unsigned short      u16;

//Type are used in texture from Wii
#define GX_TF_C4    0x8
#define GX_TF_I4    0x0
#define GX_TF_C8    0x9
#define GX_TF_I8    0x1
#define GX_TF_IA4   0x2
#define GX_TF_RGB565 0x4
#define GX_TF_RGB5A3 0x5
#define GX_TF_IA8    0x3
#define GX_TF_RGBA8  0x6

// An 8-byte DXT explicit alpha block, represents a 4x4 texel area. Used by DXT2/3
struct DXTExplicitAlphaBlock
{
	// 16 4-bit values, each 16-bit value is one row
	u16 alphaRow[4];
};

// An 8-byte DXT interpolated alpha block, represents a 4x4 texel area. Used by DXT4/5
struct DXTInterpolatedAlphaBlock
{
	// 2 alpha ranges
	u8 alpha_0;
	u8 alpha_1;
	// 16 3-bit indexes. Unfortunately 3 bits doesn't map too well to row bytes
	// so just stored raw
	u8 indexes[6];
};

typedef struct GXTEXUSERDATA
{
	GLint m_Format;
	void *alphaPointer;
} GXTEXUSERDATA;

class CSwizzleTexture
{

public :
	CSwizzleTexture() {}
	virtual ~CSwizzleTexture() {}

	void Build( ILinfo imageInfo, std::string extension, bool bForce16Bit );

private :
	void SwizzleSimpleImage(GLubyte *srcPixels, GLubyte *destPixels, GLenum format, GLuint width, GLuint height);

	void Generic_Swizzle(GLubyte *srcPixels, GLubyte *destPixels, GLuint width, GLuint height, GLuint shiftW, GLuint shiftH, GLuint blockStride, GLuint lineStride);

	void Generic_UnSwizzle(GLubyte *srcPixels, GLubyte *destPixels, GLuint width, GLuint height, GLuint shiftW, GLuint shiftH, GLuint blockStride, GLuint lineStride);

	void fastCopy(UCHAR *dst, UCHAR *src, UINT size);

	u16 FixCMPWord( u16 data );

	u16 FixEndian(u16 src);

	void unpackDXTAlpha(DXTExplicitAlphaBlock *block, u8* pCol);

	// Unpack DXT alpha into a texture for Wii to shove into another texture
	void unpackDXTAlpha(DXTInterpolatedAlphaBlock *block, u8* pCol);

	// PackTile_CMP
	//
	// pack a 2x2 tile block, each tile of 4x4 pixels, into a single 
	// 32B dst tile note: this assumes s3 algorithm pads out to a minimum 
	// block size of 4x4 pixels
	void PackTile_CMP ( u8* data, u32 tileX, u32 tileY, u16* dstPtr, GLsizei width, GLsizei height, 
						  u32 alphaSize, u8 *alphaOutput, bool bAlphaExplicit);

	// Write out comporessed image in swizzled format
	void WriteImage_CMP ( u8* data, u8* outBuffer, GLsizei width, GLsizei height, 
					 u32 alphaSize, u8 *alphaOutput, bool bAlphaExplicit );

	UCHAR cache[MAX_CACHE];
};

#endif