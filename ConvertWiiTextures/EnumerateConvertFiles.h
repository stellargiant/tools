#ifndef ENUMERATECONVERTFILES_H
#define ENUMERATECONVERTFILES_H

class CEnumerateConvertFiles : public CEnumerateFiles
{
public:
	CEnumerateConvertFiles(void);
	virtual ~CEnumerateConvertFiles(void);
	// set functions
	void SetOutputPath(TCHAR *outputPath) { m_sOutputPath = outputPath; }
	void SetNumSkipMips(unsigned int skipMips) { m_nSkipMips = skipMips; }
	void SetVerbose(bool verbose) { m_bVerbose = verbose; }
	void SetSwizzle(bool swizzle) { m_bSwizzle = swizzle; }
	void SetInfoDump(bool infoDump) { m_bInfoDump = infoDump; }
	void Set16BitMode(bool b16BitMode) { m_b16BitMode = b16BitMode; }
	void SetMaxMipSize(unsigned int maxMipSize)
	{

		m_bSkipMipSize = true;
		m_MaxMipSize = maxMipSize; 
	}

protected:
	virtual bool OnFile(LPCTSTR lpzFile);
private:
	std::string CEnumerateConvertFiles::GetExtension( TCHAR* pathName );
	CSwizzleTexture* pSwizzle;
	bool m_bSwizzle;
	TCHAR *m_sOutputPath;
	unsigned int m_nSkipMips;
	bool m_bVerbose;
	bool m_bInfoDump;
	bool m_b16BitMode;
	unsigned int m_MaxMipSize;
	bool m_bSkipMipSize;
};

#endif	//#ifndef ENUMERATECONVERTFILES_H
