// ConvertWiiTextures.cpp : Defines the entry point for the console application.
//
// Quick and Dirty conversion program for Wii to swizzle textures
// the Data of the files will be swizzled, so won't be true TGA, DDS, PNG files, etc.
//


#include "stdafx.h"
#include "ConvertWiiTextures.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// The one and only application object

CWinApp theApp;

using namespace std;
void usage(TCHAR* exeName);

int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
	int nRetCode = 0;
	int i;
	bool bRecurse = false;
	bool bAction = false;
	TCHAR *fileNames;
	CEnumerateConvertFiles enumConvFile;

	// initialize MFC and print and error on failure
	if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
	{
		// TODO: change error code to suit your needs
		_tprintf(_T("Fatal Error: MFC initialization failed\n"));
		nRetCode = 1;
	}
	else
	{
		if (argc < 2)
		{
			usage(argv[0]);
			return 1;
		}

		for (i=1; (i<argc) && (*argv[i] == '-'); i++)
		{
			TCHAR *option = argv[i];
			switch (option[1])
			{
				case 'r':
					bRecurse = true;
					break;
				case 'o':
					enumConvFile.SetOutputPath(argv[i+1]);
					i++;
					break;
				case 'm':
					enumConvFile.SetNumSkipMips(atoi(argv[i+1]));
					bAction = true;
					i++;
					break;
				case 'M':
				{
					unsigned int maxMipSize = atoi(argv[i+1]);
					if ((maxMipSize < 1) || (maxMipSize > 1024))
					{
						printf("Max Mip size should be >= 1 and <= 1024\n");
						usage(argv[0]);
						return 2;
					}
					enumConvFile.SetMaxMipSize(maxMipSize);
					bAction = true;
					i++;
				}
					break;
				case 's':
					enumConvFile.SetSwizzle(true);
					bAction = true;
					break;
				case 'v':
					enumConvFile.SetVerbose(true);
					break;
				case 'i':
					enumConvFile.SetInfoDump(true);
					bAction = true;
					break;
				case 'f':
					enumConvFile.Set16BitMode(true);
					break;
				default:
					printf("**Unknown Option %s", &option[1]);
					usage(argv[0]);
					return 1;
			}

		}

		if (i >= argc)
		{
			printf("** Must specify filename[s]");
			usage(argv[0]);
			return 1;
		}

		if (!bAction)
		{
			printf("Nothing to do!\n");
			usage(argv[0]);
			return 1;
		}

		fileNames = argv[i];

		// initialize the DevIL library
		ilInit();
		iluInit();

		// we want direct access to the DXT data
		//ilSetInteger( IL_ONLY_DXTC_DATA, IL_TRUE);
		//ilSetInteger( IL_ACCESS_DXTC_INTERNAL, IL_TRUE);

		enumConvFile.Execute(NULL, fileNames, bRecurse, true);

		ilShutDown();
		return 0;
	}

	return nRetCode;
}

void usage(TCHAR* exeName)
{
	printf("Usage: %s -r -s -i -v -o directory -m mips -M maxMipSize filename[s]\n", exeName);
	printf("\tOptions:\n");
	printf("\t\t-r Recurse subdirectories\n");
	printf("\t\t-s Swizzle and make component Wii compatible\n");
	printf("\t\t-i dump Information about DDS file\n");
	printf("\t\t-v verbose output\n");
	printf("\t\t-o output to directory specified\n");
	printf("\t\t-m number of mips to skip on output\n");
	printf("\t\t-M maxMipSize is the maximum mip dimension to write to destination\n");
	printf("\t\t-f force 32 bit texture to 16 bit\n");
	printf("\t\tfilename[s] can be wildcard\n");
}