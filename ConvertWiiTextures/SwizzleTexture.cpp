#include "StdAfx.h"


void CSwizzleTexture::Build(ILinfo imageInfo, string extension, bool bForce16)
{
	ILubyte* pDestImg = NULL;
	ILubyte* pOrigImg = NULL;
	ILenum format = imageInfo.Format;
	GLuint width = imageInfo.Width;
	GLuint height = imageInfo.Height;

	//Checking type of file, if dds need write out compressed image in swizzled format
	if ( !_tcsicmp(extension.c_str(), "dds"))
	{
		format = ilGetInteger( IL_DXTC_FORMAT );
		ILuint size = ilGetDXTCData(NULL, 0, format);
		pOrigImg = ilGetDxtcData();
		pDestImg = new ILubyte[ size ];
		memset(pDestImg, 0, size);

		u8* alphaPointer = NULL; //Just for test
		u32 alphaOffset = 0;
		
		bool bAlphaExplicit = false;

		if (format == IL_DXT3)
		{
			alphaOffset = sizeof(DXTExplicitAlphaBlock);
			bAlphaExplicit = true;
		}
		else if ( format == IL_DXT5 )
		{
			alphaOffset = sizeof(DXTInterpolatedAlphaBlock);
		}
		WriteImage_CMP ( pOrigImg, pDestImg, width, height, alphaOffset, alphaPointer, bAlphaExplicit);
		ilSetDxtcData( pDestImg );
	}
	else
	{
		pOrigImg = ilGetData();
		ILuint destSize = imageInfo.SizeOfData;
		if (bForce16)
		{
			if (format == GL_RGB)
				destSize = imageInfo.SizeOfData * 2 / 3;
			else if (format == GL_BGRA_EXT)
				destSize = imageInfo.SizeOfData * 2 / 4;
		}
		pDestImg = new ILubyte[destSize];


		int texWidth = (width + 3)/4*4;
		int texHeight = (height + 3)/4*4;

		int destFmt = GX_TF_RGBA8;
		if( format == GL_RGB )
		{
			destFmt = GX_TF_RGB565;
		}
		else if (format == GL_LUMINANCE_ALPHA)
		{
			destFmt = GX_TF_IA8;
		}

		if( format == GL_BGRA_EXT )
		{
			if (bForce16)
				destFmt = GX_TF_RGB5A3;

			// convert BGRA TO RGBA
			register unsigned int x, y;
			register unsigned char *src, *dest;
			register unsigned char r, g, b, a;
			src = pOrigImg;
			dest = pOrigImg;
			for (y=0; y<height; y++)
			{
				for (x=0; x<width; x++)
				{
					b = *src++;
					g = *src++;
					r = *src++;
					a = *src++;
					if (bForce16)
					{
						*dest++ = ((a >> 1) & 0x70) | (r >> 4);
						*dest++ = (g & 0xf0) | (b >> 4);
					}
					else
					{
						*dest++ = a;
						*dest++ = r;
						*dest++ = g;
						*dest++ = b;
					}
				}
			}
		}
		else if( format == GL_LUMINANCE_ALPHA )
		{
			// change endianess?
			register unsigned int x, y;
			register unsigned char *src, *dest;
			register unsigned char a, l;
			src = pOrigImg;
			dest = pOrigImg;
			for (y=0; y<height; y++)
			{
				for (x=0; x<width; x++)
				{
					l = *src++;
					a = *src++;
					*dest++ = a;
					*dest++ = l;
				}
			}
		}

		SwizzleSimpleImage(pOrigImg, pDestImg, destFmt, width, height);
		if (bForce16)
		{
			ILinfo imageInfo;
			iluGetImageInfo(&imageInfo);

			// create a new image
			ilTexImage(imageInfo.Width, imageInfo.Height, imageInfo.Depth, 2, imageInfo.Format,
				imageInfo.Type, pDestImg);
		}
		else
		{
			ilSetData( pDestImg );
		}

		delete [] pDestImg;
	}

}


void CSwizzleTexture::fastCopy(UCHAR *dst, UCHAR *src, UINT size)
{

	memcpy( dst, src, size );

}

void CSwizzleTexture::SwizzleSimpleImage(GLubyte *srcPixels, GLubyte *destPixels, GLenum format, GLuint width, GLuint height)
{
   GLuint blockWd, blockX, blockHt, blockY, rowStride, row, col, lineStride, blockStride, shiftW, shiftH;
   GLubyte *dest, *src;
   bool useCache;

    switch ( format )
    {
    case GX_TF_C4:
    case GX_TF_I4:
        // 8 x 8 pixel block = 32 bytes
        shiftW = 3;                            // 8 pixels per block
        shiftH = 3;                            // 8 pixels per block
        blockStride = 4;                    // block stride in bytes
        lineStride = width / 2;                // line stride in bytes

        Generic_Swizzle( srcPixels, destPixels, width, height, shiftW, shiftH, blockStride, lineStride);
        //DCFlushRange( destPixels, width * height / 2 );    // flush data cache
        break;

    case GX_TF_C8:
    case GX_TF_I8:
    case GX_TF_IA4:
        // 8 x 4 pixel block = 32 bytes
        shiftW = 3;                            // 8 pixels per block
        shiftH = 2;                            // 4 pixels per block
        blockStride = 8;                    // block stride in bytes
        lineStride = width;                    // line stride in bytes

        Generic_Swizzle( srcPixels, destPixels, width, height, shiftW, shiftH, blockStride, lineStride);
        //DCFlushRange( destPixels, width * height );    // flush data cache
        break;

    case GX_TF_RGB565:
    case GX_TF_RGB5A3:
    case GX_TF_IA8:
        // 4 x 4 blocks
        shiftW = 2;                            // 4 pixels per block
        shiftH = 2;                            // 4 pixels per block
        blockStride = 8;                    // line lineStride in bytes
        lineStride = width * 2;                // lineStride in bytes

        Generic_Swizzle( srcPixels, destPixels, width, height, shiftW, shiftH, blockStride, lineStride);
        //DCFlushRange(destPixels, width * height * 2 );    // flush data cache
        break;

    case GX_TF_RGBA8:
        // 2 x 4 x 4 pixel block = 64 bytes
        shiftW = 2;                            // 4 pixels per block
        shiftH = 2;                            // 4 pixels per block
        blockStride = 16;                    // block stride in bytes
        lineStride = width * 4;                // line stride in bytes

        blockWd = width >> shiftW;            // blocks in width
        blockHt = height >> shiftH;            // blocks in height
        rowStride = lineStride << shiftH;    // size of a cached line
        src = srcPixels;
		dest = destPixels;
		useCache = (src == dest) ? true : false;

        for ( blockY = 0; blockY < blockHt; blockY++ )
        {
			if (useCache)
			{
				fastCopy( cache, srcPixels, rowStride );
			}

            for ( blockX = 0; blockX < blockWd; blockX++ )
            {
				if (useCache)
				{
					src = &cache[ blockX * blockStride ];
				}
				else
				{
					src = srcPixels + (blockX * blockStride);
				}

                for ( row = 0; row < ((GLuint)1 << shiftH); row++ )
                {
                    for ( col = 0; col < blockStride; col += 4 )
                    {
                        // fill in two blocks at once here AR -> B1, GB -> B2
                        dest[ 0 ] = src[ col ];             // alpha
                        dest[ 1 ] = src[ col + 1 ];					// red
                        dest[ 32 ] = src[ col + 2 ];            // green
                        dest[ 32 + 1 ] = src[ col + 3];            // blue
                        dest += 2;
                    }
                    src += lineStride;
                }
                dest += 32;                        // skip over next complete block
            }
			srcPixels += rowStride;
        }
        //DCFlushRange( destPixels, width * height * 4 );    // flush data cache
        break;
    }

}

void CSwizzleTexture::Generic_Swizzle(GLubyte *srcPixels, GLubyte *destPixels, GLuint width, GLuint height, GLuint shiftW, GLuint shiftH, GLuint blockStride, GLuint lineStride)
{
    GLuint blockWd, blockHt, rowStride, blockX, blockY, row, col;
    GLubyte *src, *dest;
	bool useCache = false;

    blockWd = width >> shiftW;            // blocks in width
    blockHt = height >> shiftH;            // blocks in height
    rowStride = lineStride << shiftH;    // size of a cached line
    dest = destPixels;
	if (srcPixels == destPixels)
	{
		useCache = true;
	}

    for ( blockY = 0; blockY < blockHt; blockY++ )
    {
		if (useCache)
		{
			fastCopy( srcPixels, cache, rowStride );
		}

        for ( blockX = 0; blockX < blockWd; blockX++ )
        {
			if (useCache)
			{
				src = &cache[ blockX * blockStride ];
			}
			else
			{
				src = srcPixels + (blockX * blockStride);
			}

            for ( row = 0; row < ((GLuint)1 << shiftH); row++ )
            {
                for ( col = 0; col < blockStride; col++ )
                {
                    dest[ col ] = src[ col ];
                }
                dest += blockStride;
                src += lineStride;
            }
        }
		srcPixels += rowStride;
    }
}

void CSwizzleTexture::Generic_UnSwizzle(GLubyte *srcPixels, GLubyte *destPixels, GLuint width, GLuint height, GLuint shiftW, GLuint shiftH, GLuint blockStride, GLuint lineStride)
{
    GLuint blockWd, blockHt, rowStride, blockX, blockY, row, col;
    GLubyte *src, *dest, *temp;
	bool useCache = false;

    blockWd = width >> shiftW;            // blocks in width
    blockHt = height >> shiftH;            // blocks in height
    rowStride = lineStride << shiftH;    // size of a cached line
    src = srcPixels;
	if (srcPixels == destPixels)
	{
		useCache = true;
	}

    for ( blockY = 0; blockY < blockHt; blockY++ )
    {
        temp = src;
        for ( blockX = 0; blockX < blockWd; blockX++ )
        {
			if (useCache)
			{
				dest = &cache[ blockX * blockStride ];
			}
			else
			{
				dest = destPixels + (blockX * blockStride);
			}

            for ( row = 0; row < ((GLuint)1 << shiftH); row++ )
            {
                for ( col = 0; col < blockStride; col++ )
                {
                    dest[ col ] = src[ col ];
                }
                dest += lineStride;
                src += blockStride;
            }
        }

		if (useCache)
		{
			fastCopy( temp, cache, rowStride );
		}
		else
		{
			destPixels += rowStride;
		}
    }
}

u16 CSwizzleTexture::FixCMPWord( u16 data )
{
	// reverse tuple order within bytes
	return (( (data & 0x3 )   << 6 ) |
			( (data & 0xC )   << 2 ) |
			( (data & 0x30)   >> 2 ) |
			( (data & 0xC0)   >> 6 ) |

            ( (data & 0x300 ) << 6 ) |
			( (data & 0xC00 ) << 2 ) |
			( (data & 0x3000) >> 2 ) |
			( (data & 0xC000) >> 6 )) ;
}

u16 CSwizzleTexture::FixEndian(u16 src)
{
	return ((src << 8) | (src >> 8));
}

void CSwizzleTexture::unpackDXTAlpha(DXTExplicitAlphaBlock *block, u8* pCol)
{
	// This is an explicit alpha block, 4 bits per pixel, LSB first
	for (size_t row = 0; row < 4; ++row)
	{
		// Shift and mask off to 4 bits
		*pCol++ = (block->alphaRow[row] >> 8) & 0xf0;
		*pCol++ = (block->alphaRow[row] >> 4) & 0xf0;
		*pCol++ = (block->alphaRow[row] << 0) & 0xf0;
		*pCol++ = (block->alphaRow[row] << 4) & 0xf0;
		// skip to next row for this tile
		pCol += 4;

	}
}

void CSwizzleTexture::unpackDXTAlpha(DXTInterpolatedAlphaBlock *block, u8* pCol)
{
	// 8 derived alpha values to be indexed
	u8 derivedAlphas[8];

	// Explicit extremes
	derivedAlphas[0] = block->alpha_0;
	derivedAlphas[1] = block->alpha_1;
	
	
	if (derivedAlphas[0] > derivedAlphas[1])
	{
		// 8-alpha block:  derive the other six alphas.    
		// Bit code 000 = alpha_0, 001 = alpha_1, others are interpolated.
		derivedAlphas[2] = (6 * derivedAlphas[0] + 1 * derivedAlphas[1] + 3) / 7;	// bit code 010
		derivedAlphas[3] = (5 * derivedAlphas[0] + 2 * derivedAlphas[1] + 3) / 7;	// bit code 011
		derivedAlphas[4] = (4 * derivedAlphas[0] + 3 * derivedAlphas[1] + 3) / 7;	// bit code 100
		derivedAlphas[5] = (3 * derivedAlphas[0] + 4 * derivedAlphas[1] + 3) / 7;	// bit code 101
		derivedAlphas[6] = (2 * derivedAlphas[0] + 5 * derivedAlphas[1] + 3) / 7;	// bit code 110
		derivedAlphas[7] = (1 * derivedAlphas[0] + 6 * derivedAlphas[1] + 3) / 7;	// bit code 111
	}
	else {
		// 6-alpha block.
		// Bit code 000 = alpha_0, 001 = alpha_1, others are interpolated.
		derivedAlphas[2] = (4 * derivedAlphas[0] + 1 * derivedAlphas[1] + 2) / 5;	// Bit code 010
		derivedAlphas[3] = (3 * derivedAlphas[0] + 2 * derivedAlphas[1] + 2) / 5;	// Bit code 011
		derivedAlphas[4] = (2 * derivedAlphas[0] + 3 * derivedAlphas[1] + 2) / 5;	// Bit code 100
		derivedAlphas[5] = (1 * derivedAlphas[0] + 4 * derivedAlphas[1] + 2) / 5;	// Bit code 101
		derivedAlphas[6] = 0x00;										// Bit code 110
		derivedAlphas[7] = 0xFF;										// Bit code 111		
	}

	// Ok, now we've built the reference values, process the indexes
	for (size_t row = 0; row < 4; ++row)
	{
		for (size_t col=0; col < 4; ++col)
		{
			size_t baseByte = (((row * 4)+col) * 3) / 8;
			size_t baseBit = (((row * 4)+col) * 3) % 8;
			u8 bits = (u8)(block->indexes[baseByte] >> baseBit & 0x7);
			// do we need to stitch in next byte too?
			if (baseBit > 5)
			{
				u8 extraBits = (u8)((block->indexes[baseByte+1] << (8 - baseBit)) & 0xFF);
				bits |= extraBits & 0x7;
			}
			*pCol++ = derivedAlphas[bits];
		}
		// skip to next row for this tile
		pCol += 4;
	}

}

void CSwizzleTexture::PackTile_CMP ( u8* data, u32 tileX, u32 tileY, u16* dstPtr, GLsizei width, GLsizei height, 
						  u32 alphaSize, u8 *alphaOutput, bool bAlphaExplicit)
{
	u32  y;
	u16* srcPtr;
	u32  srcTileOffset;
	u32  subTileRows, subRowShorts;    // number of s3 4x4 tiles
	u32  srcPadWidth, srcPadHeight;
	u16* buffPtr;
	u8 *alphaDest;

	// set the padded size of the s3 source image out to a 4-texel boundary
//	srcPadWidth  = ( (layer->width  + 3) >> 2 );
//	srcPadHeight = ( (layer->height + 3) >> 2 );
	srcPadWidth  = ( (width  + 3) >> 2 );
	srcPadHeight = ( (height + 3) >> 2 );

	// number of bytes in a single row of 4x4 texel source tiles
	srcTileOffset = srcPadWidth * (8 + alphaSize);

	// number of 4x4 (source) tile rows to copy ( will be 1 or 2 )
	subTileRows = 2;
	if( (srcPadHeight - tileY) < 2 )
		subTileRows = 1;

	// number of 4x4 tile Rows to copy translated into number of short values
	// ( will be 4 or 8 )
	subRowShorts = 8;
	if( (srcPadWidth - tileX) < 2 )
		subRowShorts = 4;

	for( y=0; y < subTileRows; y++ )
	{
		srcPtr  = (u16*)( (u8*)(data) + ((tileY + y) * srcTileOffset) + (tileX*(8 + alphaSize)) ); 
		buffPtr = ( dstPtr + (y * 8) );        // 16 bytes per subRow = 8 shorts
		// subtile rows 4 * 4 byte per subTile Column
		alphaDest = (alphaOutput + (y * (srcPadWidth * 16)));	

		// output alpha bytes to alpha output.
		if (alphaOutput)
		{
			if (bAlphaExplicit)
				unpackDXTAlpha((DXTExplicitAlphaBlock *)srcPtr, alphaDest);
			else
				unpackDXTAlpha((DXTInterpolatedAlphaBlock *)srcPtr, alphaDest);
			alphaDest += 4;
		}

		// skip alpha if there
		srcPtr += (alphaSize / 2);
		// color table entries - switch bytes within a 16-bit world only
		*buffPtr++ = FixEndian( *srcPtr);
		srcPtr++;
		*buffPtr++ = FixEndian( *srcPtr);
		srcPtr++;
		// 2-bit color tuples;
		// reverse tuple order within bytes of a word
		*buffPtr++ = FixCMPWord( *srcPtr );
		srcPtr++;
		*buffPtr++ = FixCMPWord( *srcPtr );
		srcPtr++;

		if (subRowShorts == 8)
		{
			// output alpha bytes to alpha output.
			if (alphaOutput)
			{
				if (bAlphaExplicit)
					unpackDXTAlpha((DXTExplicitAlphaBlock *)srcPtr, alphaDest);
				else
					unpackDXTAlpha((DXTInterpolatedAlphaBlock *)srcPtr, alphaDest);
			}

			// skip alpha if there
			srcPtr += (alphaSize / 2);
			// color table entries - switch bytes within a 16-bit world only
			*buffPtr++ = FixEndian( *srcPtr);
			srcPtr++;
			*buffPtr++ = FixEndian( *srcPtr);
			srcPtr++;
			// 2-bit color tuples;
			// reverse tuple order within bytes of a word
			*buffPtr++ = FixCMPWord( *srcPtr );
			srcPtr++;
			*buffPtr++ = FixCMPWord( *srcPtr );
			srcPtr++;
		}
	} // end for( subTileRows )
}

void CSwizzleTexture::WriteImage_CMP ( u8* data, u8* outBuffer, GLsizei width, GLsizei height, 
					 u32 alphaSize, u8 *alphaOutput, bool bAlphaExplicit )
{
	u32 tileRow, tileCol;
	u32 srcTileRows, srcTileCols;
	u16* dstPtr;
	u8 *alphaDestPtr;

	// each source tile is 4x4 pixels, 8B
	srcTileRows   = ((height + 3) >> 2);
	srcTileCols   = ((width  + 3) >> 2);

	dstPtr = (u16*)(outBuffer);

	// each dst tile is 2x2 source tiles, so move by 2 each iteration
	for(tileRow = 0; tileRow < srcTileRows; tileRow += 2 )
	{
		alphaDestPtr = alphaOutput;
		for(tileCol = 0; tileCol < srcTileCols; tileCol += 2 )
		{
			PackTile_CMP( data, tileCol, tileRow, dstPtr, width, height, alphaSize, alphaDestPtr, bAlphaExplicit);
			dstPtr += 16; // 32B per dst tile, short ptr
			// bump alpha pointer for next column 
			if (alphaDestPtr)
			{
				// bump past two columns 4x8 pixel tiles
				alphaDestPtr += 32;
			}
		}

		if (alphaOutput)
		{
			// bump alpha output past 2x2 destination tile 
			// 8 pixel rows +  pixel width (srcTileCols * 4)
			alphaOutput += (srcTileCols << 5);
		}
	}
}