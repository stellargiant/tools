#include "StdAfx.h"

CEnumerateConvertFiles::CEnumerateConvertFiles(void)
{
	m_bSwizzle = false;
	m_sOutputPath = NULL;
	m_nSkipMips = 0;
	m_bVerbose = false;
	m_bInfoDump = false;
	m_b16BitMode = false;
	m_bSkipMipSize = false;
	m_MaxMipSize = 1024;

	pSwizzle = new CSwizzleTexture();
}

CEnumerateConvertFiles::~CEnumerateConvertFiles(void)
{
	if ( pSwizzle != NULL )
	{
		delete pSwizzle;
		pSwizzle = NULL;
	}
}

bool CEnumerateConvertFiles::OnFile(LPCTSTR lpzFile)
{
	ILuint texIDDest;
	ILuint texIDSrc;

	TCHAR ext[_MAX_EXT];
	TCHAR baseFileName[_MAX_FNAME];
	TCHAR pathName[_MAX_PATH];
	TCHAR *dxtFormatStr = _T("No");

	_tsplitpath(lpzFile, NULL, NULL, baseFileName, ext);
	if (m_sOutputPath != NULL)
	{
		_stprintf(pathName, _T("%s\\%s%s"), m_sOutputPath, baseFileName, ext);
	}
	else
	{
		_stprintf(pathName, _T("%s_%s"), baseFileName, ext);
	}
	
	ilSetInteger( IL_ONLY_DXTC_DATA, IL_TRUE );
	ilSetInteger(IL_KEEP_DXTC_DATA, IL_TRUE);

	ilEnable(IL_ORIGIN_SET); 
	ilSetInteger(IL_ORIGIN_MODE, IL_ORIGIN_UPPER_LEFT);

	if ((texIDSrc = iluLoadImage((const ILstring)lpzFile)) != 0)
	{
		ILinfo curImageInfo;
		iluGetImageInfo(&curImageInfo);

		if (m_bInfoDump)
		{
			_tprintf(_T("File: %s\n %s Width: %d  Height: %d  Depth: %d  Bpp: %d Format: %d Mips: %d\n"),
					lpzFile,
					dxtFormatStr,
					curImageInfo.Width,
					curImageInfo.Height,
					curImageInfo.Depth,
					curImageInfo.Bpp,
					curImageInfo.Format,
					curImageInfo.NumMips);

			// nothing more to do?
			if (!m_bSwizzle && (m_nSkipMips == 0) && !m_bSkipMipSize)
			{
				iluDeleteImage( texIDSrc );
				return true;
			}
		}

		ILuint numMipmaps = ilGetInteger( IL_NUM_MIPMAPS );

		// default to src images
		texIDDest = texIDSrc;
		unsigned int imagesToSkip = m_nSkipMips;
		if (m_bSkipMipSize)
		{
			unsigned int mipWidth, mipHeight;
			mipWidth = curImageInfo.Width;
			mipHeight = curImageInfo.Height;

			while ((mipWidth > m_MaxMipSize) || (mipHeight > m_MaxMipSize))
			{
				mipWidth >>= 1;
				if (mipWidth == 0)
					mipWidth = 1;
				mipHeight >>= 1;
				if (mipHeight == 0)
					mipHeight = 1;

				imagesToSkip++;
			}
		}

		if (imagesToSkip > 0)
		{

			if (imagesToSkip > numMipmaps)
			{
				if (m_bVerbose)
					_tprintf(_T("File: %s\n Can't skip %d mips, only has %d images in file\n"),
						lpzFile, imagesToSkip, numMipmaps+1);
				imagesToSkip = 0;
			}
			else
			{
				// output new texID
				texIDDest = iluGenImage();

				if (imagesToSkip < numMipmaps)
				{
					ilRegisterMipNum(numMipmaps-imagesToSkip);
				}
			}
		}

		for (ILuint i = 0; i <= numMipmaps; i++) 
		{
			ilBindImage( texIDSrc );	// Set to parent image first.
			ilActiveMipmap(i);
			
			ILinfo imageInfo;
			iluGetImageInfo(&imageInfo);

			if (m_bSwizzle)
				pSwizzle->Build( imageInfo,  GetExtension( pathName ), m_b16BitMode);


			// if we are skipping images, then do it.
			if ((imagesToSkip > 0) && (i >= imagesToSkip))
			{
				ILenum format = ilGetInteger( IL_DXTC_FORMAT );
				ILuint size = ilGetDXTCData(NULL, 0, format);
				ILubyte *pOrigImg = ilGetDxtcData();

				if (pOrigImg == NULL)
				{
					printf("No DXTX data? in %s\n", __FUNCTION__);
					exit(1);
				}

				ilBindImage(texIDDest);
				ilActiveMipmap(i-imagesToSkip);

				ilTexSubImageDxtc(imageInfo.Width, imageInfo.Height, imageInfo.Depth, format, pOrigImg);
			}
		}

		ilBindImage( texIDDest );	// Set to parent image first.
		ilActiveMipmap(0);

		if (m_bSwizzle)
			ilSetInteger( IL_IMAGE_EXTRA_FLAG_SWIZZLED, IL_TRUE);


		ilSaveImage((const ILstring)pathName);

		if (m_bVerbose)
		{
			_tprintf(_T("File: %s\n DXT: %s Width: %d  Height: %d  Depth: %d  Bpp: %d Format: %d\n"),
					lpzFile,
					dxtFormatStr,
					curImageInfo.Width,
					curImageInfo.Height,
					curImageInfo.Depth,
					curImageInfo.Bpp,
					curImageInfo.Format);

			if (texIDSrc != texIDDest)
			{
				ILinfo imageInfo;
				iluGetImageInfo(&imageInfo);
				_tprintf(_T("Output File: %s\n DXT: %s Width: %d  Height: %d  Depth: %d  Bpp: %d Format: %d\n"),
						pathName,
						dxtFormatStr,
						imageInfo.Width,
						imageInfo.Height,
						imageInfo.Depth,
						imageInfo.Bpp,
						imageInfo.Format);
			}
		}

		iluDeleteImage( texIDSrc );
		if (texIDSrc != texIDDest)
			iluDeleteImage(texIDDest);
		}
	else
	{
		_tprintf(_T("File: %s Load error - %s\n"), lpzFile, iluErrorString(ilGetError()));
	}

	return true;
}

string CEnumerateConvertFiles::GetExtension( TCHAR* pathName )
{
	string extension;
	string filename(pathName);
	string::size_type idx = filename.rfind('.');
	

	if(idx != std::string::npos)
		extension = filename.substr( idx + 1 );

	return extension;
}
