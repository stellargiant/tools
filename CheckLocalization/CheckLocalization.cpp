// CheckLocalization.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "CheckLocalization.h"
#include <map>
#include <string>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif



// The one and only application object

CWinApp theApp;

using namespace std;

typedef map<unsigned long, wstring> STRINGMAP;


bool ParseFile(LPCTSTR fileName, STRINGMAP &strMap)
{
	FILE *inFile;
	int errorNo;
	unsigned long ID;
	TCHAR tempLine[4096];
	TCHAR tempString[4096];
	wstring theString;

	errorNo = _wfopen_s(&inFile, fileName, _T("r+t, ccs=UNICODE"));
	if (inFile == NULL)
	{
		_tprintf(_T("Error opening file %s, error %d"), fileName, errorNo);
		return false;
	}

	while (!feof(inFile))
	{
		fgetws(tempLine, 4096, inFile);	
		if (swscanf_s(tempLine, _T("%lu %4095[^\0]"), &ID, tempString, 4096) == 2)
		{
			theString = tempString;
			strMap[ID] = theString;
		}
	}

	fclose(inFile);

	return true;
}

void usage(LPTSTR exeName)
{
	_tprintf(_T("usage %s: -d <file1> <file2>\n"), exeName);
	_tprintf(_T("\t-d dump first file instead of comparing\n"));
	_tprintf(_T("\t-c Compare Strings an show non-matching\n"));
}

int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
	STRINGMAP smap1;
	STRINGMAP smap2;

	int nRetCode = 0;
	bool dumpFile = false;
	bool compLines = false;

	// initialize MFC and print and error on failure
	if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
	{
		// TODO: change error code to suit your needs
		_tprintf(_T("Fatal Error: MFC initialization failed\n"));
		nRetCode = 1;
	}
	else
	{
		if (argc < 3)
		{
			usage((LPTSTR)argv[0]);
			nRetCode = 2;
		}
		else
		{
			int startArg = 1;
			if (*argv[startArg] == L'-')
			{
				switch (*(argv[startArg]+1))
				{
			
					case L'd':
						dumpFile = true;
						startArg++;
						break;
					case L'c':
						compLines = true;
						startArg++;
						break;
					default:
						usage((LPTSTR)argv[0]);
						return 2;
				}
			}

			if (!ParseFile(argv[startArg], smap1))
			{
				_tprintf(_T("Can't parse file %s\n"), argv[startArg]);
				nRetCode = 3;
			}
			else if (!dumpFile && !ParseFile(argv[++startArg], smap2))
			{
				_tprintf(_T("Can't parse file %s\n"), argv[startArg]);
				nRetCode = 4;
			}
			else
			{

				STRINGMAP::iterator smIter;
				STRINGMAP::iterator smIter2;
				for (smIter = smap1.begin(); smIter != smap1.end(); ++smIter)
				{
					if (dumpFile)
					{
						_tprintf(_T("%lu\t%s\n"), smIter->first, smIter->second.c_str());
					}
					else
					{
						smIter2 = smap2.find(smIter->first);
						if (smIter2 == smap2.end())
						{
							_tprintf(_T("Can't find id %lu with string %s in second file\n"), 
								smIter->first, smIter->second.c_str());

						}
						else if (compLines)
						{
							if (_tcscmp(smIter->second.c_str(), smIter2->second.c_str()))
							{
								_tprintf(_T("id %lu doesn't match.\n1st:%s\n2nd:%s\n"), 
									smIter->first, smIter->second.c_str(), smIter2->second.c_str());
							}
						}
					}
				}

				if (!dumpFile)
				{
					for (smIter = smap2.begin(); smIter != smap2.end(); ++smIter)
					{
						if (smap1.find(smIter->first) == smap1.end())
						{
							_tprintf(_T("Can't find id %ul with string %s in first file\n"), 
								smIter->first, (smIter->second).c_str());

						}
					}
				}

			}

		}
	}

	return nRetCode;
}
